#include <dysprosium/file>
#include <dysprosium/dysexception>

namespace dys
{

// codesearch File
File::File (FILE* f): f (f), mtx (new (std::nothrow) std::recursive_mutex)
{
    PushObjectIntoGC;
    if (this -> mtx == nullptr) throw bad_alloc;
}
File::File (const char* name): File (name, "r") {}
File::File (const char* name, const char* mode): File (fopen (name, mode)) {}
File::File (String* name): File (name -> get (), "r") {}
File::File (String* name, const char* mode): File (name -> get (), mode) {}
File::File (const char* name, String* mode): File (name, mode -> get ()) {}
File::File (String* name, String* mode): File (name -> get (), mode -> get ()) {}
FILE* File::get (void) { return this -> f; }
File::~File (void)
{
    fclose (this -> f);
}
ObjectName (File, dysprosium.File);
FatherDefine (File)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
FatherAlloc (File);
void File::lock (void) { this -> mtx -> lock (); }
void File::unlock (void) { this -> mtx -> unlock (); }

}
