#include <dysprosium>
#include <biarray>
#include <dysexception>
#include <cstdlib>

namespace dys
{

ObjectName (BiArray, "dysprosium.BiArray");
GCDefine (BiArray)
{
    long i, mx = (long) (this->h) * (this->w);
    for (i=0; i<mx; i++)
    {
        auto o = this -> data [i];
        if (o != nullptr) o -> GC_mark ();
    }
}
FatherDefine (BiArray)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
FatherAlloc (BiArray);
BiArray::BiArray (int h, int w): h (h), w (w), data ((Object**) (std::calloc (h * w, sizeof (Object*))))
{
    if (this -> data == nullptr) throw bad_alloc;
}
Object** BiArray::operator[] (int l)
{
    return this -> data + l * this -> w;
}
Object* BiArray::operator () (int l, int c)
{
    return this -> data [l * this -> w + c];
}
Object** BiArray::operator () (int l)
{
    return (*this) [l];
}
BiArray::~BiArray (void)
{
    free (this -> data);
}

}
