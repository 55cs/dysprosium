#include <dysexception>
#include <cstring>

namespace dys
{

FatherDefine (String)
{
    FatherStart;
    Father (Hashable);
    FatherEnd;
}
String::String (const char* content): length (strlen (content)), info (new (std::nothrow) char [this -> length])
{
    PushObjectIntoGC;
    if (this -> info == nullptr) throw (bad_alloc);
    this -> info [this -> length] = 0;
    memcpy (this -> info, content, this -> length);
}
String::String (String* pr): String (pr -> info) {}
String::~String (void) { delete this -> info; }
FatherAlloc (String);
const char* String::get (void) { return this -> info; }
int String::len (void) { return this -> length; }
ObjectName (String, dysprosium.String);
ull String::hash (void)
{
    int i;
    ull res = 7301142117310164633ull; // 随便写的xd
    for (i=0; i<this->length; i++) res ^= ((ull) ((this -> info) [i])) << ((i % 8) * 8);
    return res;
}

std::ostream& operator<< (std::ostream& o, dys::String*& s)
{
    o << s -> info;
    return o;
}

}
