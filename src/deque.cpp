#include <dysprosium/dysdeque>
#include <dysprosium/dysexception>

#include <cstring>
using std::memcpy;

namespace dys
{

FatherAlloc (Deque);
FatherDefine (Deque)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
ObjectName (Deque, dysprosium.Deque);
GCDefine (Deque)
{
    GCMarkStatement;
    this -> mem_change_mtx -> lock ();
    int bk = this -> back, fr = this -> front, size = this -> units * deque_unit_size;
    if (fr == bk and this -> num == 0) /* No member */ goto gc_ret;
    if (bk < fr) bk += size;
    for (auto i=fr; i<bk; i++) this -> data [i % size] -> GC_mark ();
    gc_ret:
    this -> mem_change_mtx -> unlock ();
}
#define DEFAULT_DEQUE_UNITS 2
Deque::Deque (void): Deque (DEFAULT_DEQUE_UNITS) {}
Deque::Deque (int units): units (units), data ((deque_object*) (std::malloc (deque_unit_space * units))),
    mem_change_mtx (new (std::nothrow) std::recursive_mutex), front (0), back (0), num (0)
{
    if (this -> data == nullptr or this -> mem_change_mtx == nullptr) throw bad_alloc;
}
Deque::~Deque (void)
{
    free (this -> data);
    delete this -> mem_change_mtx;
}
void Deque::clear (void)
{
    this -> mem_change_mtx -> lock ();
    this -> front = this -> back = this -> num = 0;
    this -> mem_change_mtx -> unlock ();
}
int Deque::len (void)
{
    return this -> num;
}
deque_object Deque::pop_front (void)
{
    if (this -> num == 0) return nullptr;
    this -> mem_change_mtx -> lock ();
    auto ret = this -> data [this -> front ++];
    if (this -> front == this -> units * deque_unit_size) this -> front = 0;
    this -> num --;
    this -> mem_change_mtx -> unlock ();
    return ret;
}
deque_object Deque::pop_back (void)
{
    if (this -> num == 0) return nullptr;
    this -> mem_change_mtx -> lock ();
    auto ret = this -> data [this -> back --];
    if (this -> back == 0 and this -> num > 1) this -> back = this -> units * deque_unit_size;
    this -> num --;
    this -> mem_change_mtx -> unlock ();
    return ret;
}
void Deque::push_front (deque_object obj)
{
    this -> mem_change_mtx -> lock ();
    if (this -> num == this -> units * deque_unit_size)
    {
        // Reallocating
        this -> units ++;
        auto new_mem = (deque_object*) (malloc (this -> units * deque_unit_space));
        if (new_mem == nullptr) throw bad_alloc;
        // Copying the objects
        memcpy (new_mem + 1, this -> data + this -> front, (this -> num - this -> front) * sizeof (deque_object));
        memcpy (new_mem + this -> num - this -> front + 1, this -> data, this -> front * sizeof (deque_object));
        free (this -> data);
        this -> data = new_mem;
        this -> front = 0;
        this -> back = (this -> units - 1) * deque_unit_size + 1;
    }
    else
    {
        if (this -> front == 0)
        {
            this -> front = this -> units * deque_unit_size;
        }
        else this -> front --;
    }
    this -> data [this -> front] = obj;
    this -> num ++;
    this -> mem_change_mtx -> unlock ();
}
void Deque::push_back (deque_object obj)
{
    this -> mem_change_mtx -> lock ();
    if (this -> num == this -> units * deque_unit_size)
    {
        // Reallocating
        this -> units ++;
        auto new_mem = (deque_object*) (malloc (this -> units * deque_unit_space));
        if (new_mem == nullptr) throw bad_alloc;
        // Copying the objects
        memcpy (new_mem, this -> data + this -> front, (this -> num - this -> front) * sizeof (deque_object));
        memcpy (new_mem + this -> num - this -> front, this -> data, this -> front * sizeof (deque_object));
        free (this -> data);
        this -> data = new_mem;
        this -> front = 0;
        this -> back = (this -> units - 1) * deque_unit_size;
    }
    else if (this -> back == this -> units * deque_unit_size) this -> back = 0;
    this -> data [this -> back ++] = obj;
    this -> num ++;
    this -> mem_change_mtx -> unlock ();
}

}
