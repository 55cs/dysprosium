#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <dysprosium/dysprosium>
#include <dysprosium/dysexception>

//#include <iostream>

namespace dys
{

// codesearch GarbageCollector codesearch gc
std::vector <MarkEntrypoint> GarbageCollector::entrypoints;
std::unordered_map <Object*, long long> GarbageCollector::marks;
std::unordered_set <Object*> GarbageCollector::eternal;
long long GarbageCollector::cur;
void GarbageCollector::start (void)
{
    for (auto entrypoint : entrypoints) entrypoint ();
    for (auto et : eternal) et -> GC_mark ();
}
bool GarbageCollector::mark (Object* obj)
{
    if (marks [obj] > cur) return true;
    marks [obj] = cur + 1;
    return false;
}
void GarbageCollector::add (Object* obj) { marks [obj] = cur; }
void GarbageCollector::registry (MarkEntrypoint point) { entrypoints.push_back (point); }
void GarbageCollector::clear (void)
{
    //int i = 0;
    std::vector <Object*> clr;
    for (auto [k, v:> : marks)
    {
    if (v < cur - 3)
    {
        //delete k;
        //std::cout << " dl v " << v << std::endl;
        clr.push_back (k);
    }
    //else std::cout << "no " << (++i) << std::endl;
    }
    cur++;
    for (auto k : clr)
    {
        delete k;
        marks.erase (k);
    }
}
void GarbageCollector::add_eternal (Object* obj)
{
    eternal.insert (obj);
}

// codesearch Object
GCDefine (Object)
{
    GCMarkStatement;
}
FatherDefine (Object)
{
    if (not $dy_fathdatacalled) $dy_fathdata.insert (object_class_name ());
    return $dy_fathdata;
}
bool Object::instanceof (const char* name)
{
    return this -> $dy_fathers () .count (name);
}
ObjectName (Object, dysprosium.Object);
Object::Object (void)
{
    PushObjectIntoGC;
}
Object::~Object (void) {} // You should only delete things like vector* but not things like Object*. GC will handle it.
FatherAlloc (Object);

// codesearch hashable
ObjectName (Hashable, dysprosium.Hashable);
FatherDefine (Hashable)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
ull Hashable::hash (void)
{
    return 0ull;
}
FatherAlloc (Hashable);

// codesearch TempObject codesearch temp codesearch example
// This class shows how instanceof work
// And how the gc works
ObjectName (TempObject, dysprosium.TempObject);
FatherDefine (TempObject)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
TempObject::TempObject (void)
{
    PushObjectIntoGC;
}
GCDefine (TempObject)
{
    GCMarkStatement;
    GCMarkObj (this -> ref);
}
FatherAlloc (TempObject);

// codesearch integer
ObjectName (Integer, dysprosium.Integer);
FatherDefine (Integer)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
Integer::Integer (int data): data (data)
{
    PushObjectIntoGC;
}
Integer::Integer (Integer* i): Integer (i -> data) {}
int Integer::get (void) { return this -> data; }
void Integer::set (int data) { this -> data = data; }
FatherAlloc (Integer);

// codesearch float codesearch double
ObjectName (Double, dysprosium.Double);
FatherAlloc (Double);
FatherDefine (Double)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
Double::Double (double d): data (d)
{
    PushObjectIntoGC;
}
Double::Double (Double* d): Double (d -> data) {}
double Double::get (void) { return this -> data; }
void Double::set (double d) { this -> data = d; }

// codesearch memory
FatherAlloc (MemoryPiece);
FatherDefine (MemoryPiece)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
ObjectName (MemoryPiece, dysprosium.MemoryPiece)
MemoryPiece::MemoryPiece (size_t size): size (size), piece (new unsigned char [size])
{
    PushObjectIntoGC;
}
unsigned char* MemoryPiece::get (void)
{
    return this -> piece;
}
unsigned char MemoryPiece::operator[] (int index)
{
    return this -> piece [index];
}
MemoryPiece::~MemoryPiece (void)
{
    delete this -> piece;
}

// codesearch initialize
void initialize (void)
{
    static bool initialized = false;
    if (initialized) return;
    initialized = true;
    bad_alloc = new Exception ("Program out of memory");
    GarbageCollector::add_eternal ((Object*) (bad_alloc));
}

}
