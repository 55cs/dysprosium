#include <dysexception>

namespace dys
{

FatherAlloc (Exception);
ObjectName (Exception, dysprosium.Exception);
GCDefine (Exception)
{
    GCMarkStatement;
    this -> info -> GC_mark ();
}
FatherDefine (Exception)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
Exception::Exception (const char* name): Exception (new (std::nothrow) String (name))
{
}
Exception::Exception (String* name): info (name)
{
    PushObjectIntoGC;
    if (this -> info == nullptr) throw bad_alloc;
}
Exception* bad_alloc = nullptr;

std::ostream& operator<< (std::ostream& o, Exception*& e)
{
    o << "Dysprosium: Error occured: " << e -> info;
    return o;
}

}
