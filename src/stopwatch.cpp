#include <dysprosium/stopwatch>

namespace dys
{

ObjectName (StopWatch, dysprosium.StopWatch);
FatherDefine (StopWatch)
{
    FatherStart;
    Father (Object);
    FatherEnd;
}
FatherAlloc (StopWatch);

StopWatch::StopWatch (size_t unit): started (false), unit (unit)
{
    PushObjectIntoGC;
}

void StopWatch::start (void)
{
    this -> startt = hd_clock::now ();
    this -> started = true;
}

double StopWatch::now (void)
{
    if (this -> started) return (double) ((hd_clock::now () - this -> startt) .count ()) / ((double) (this -> unit));
    else return 0;
}

}
