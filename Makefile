compile_option = -I. -Idysprosium -fPIC -std=gnu++23 -O3

dysprosium.so: dysprosium.o biarray.o exception.o file.o string.o deque.o stopwatch.o
	g++ -shared dysprosium.o biarray.o exception.o file.o string.o deque.o stopwatch.o -o dysprosium.so

dysprosium.o: src/dysprosium.cpp
	g++ -c src/dysprosium.cpp -o dysprosium.o $(compile_option)

deque.o: src/deque.cpp
	g++ -c src/deque.cpp -o deque.o -mavx $(compile_option)

biarray.o: src/biarray.cpp
	g++ -c src/biarray.cpp -o biarray.o $(compile_option)

file.o: src/file.cpp
	g++ -c src/file.cpp -o file.o $(compile_option)

exception.o: src/exception.cpp
	g++ -c src/exception.cpp -o exception.o $(compile_option)

string.o: src/string.cpp
	g++ -c src/string.cpp -o string.o $(compile_option)

stopwatch.o: src/stopwatch.cpp
	g++ -c src/stopwatch.cpp -o stopwatch.o $(compile_option)

test.test: test.cpp dysprosium.so
	g++ test.cpp ./dysprosium.so -o test.test $(compile_option)

.PHONY: clean install test
clean:
	-rm *.o
cleantest:
	-rm *.test *.out
cleanall: clean cleantest
	-rm *.dll *.so *.a *.run
test: test.test
install: dysprosium.so # requires sudo
	-rm -r /usr/include/dysprosium
	-rm /lib/libdys.so
	cp -r dysprosium /usr/include/
	cp dysprosium.so /lib/libdys.so

# TODO: Windows makefile
