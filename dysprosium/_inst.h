#define FatherData std::unordered_set <const char*> $dy_fathdata; static bool $dy_fathdatacalled
#define FatherAlloc(obj) std::unordered_set <const char*> obj::$dy_fathdata; bool obj::$dy_fathdatacalled
#define FatherStart if ($dy_fathdatacalled == false) {$dy_fathdatacalled = true; $dy_fathdata.insert (this -> object_class_name ())
#define Father(f) for (auto k : f::$dy_fathers ()) $dy_fathdata.insert (k);
#define FatherEnd } return $dy_fathdata
#define FatherDefine(obj) std::unordered_set<const char*>& obj::$dy_fathers (void)
#define FatherImplementation virtual std::unordered_set<const char*>& $dy_fathers (void)
#define isinstance(obj,name) (obj -> instanceof (#name))

#define ObjectName(obj,s) const char* obj::object_class_name (void) { return #s; }
#define NameImplementation static const char* object_class_name (void);

#define GCMarkStatement if (GarbageCollector::mark (this)) return
#define GCMarkObj(obj) if (obj != nullptr) obj -> GC_mark ()
#define PushObjectIntoGC dys::GarbageCollector::add (this)
#define GCImplementation virtual void GC_mark (void)
#define GCDefine(obj) void obj::GC_mark (void)
