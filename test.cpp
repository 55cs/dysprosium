#include <bits/stdc++.h>
#include <dysprosium/dysprosium>
#include <dysprosium/stopwatch>
using namespace std;
using namespace dys;
unordered_map <int, Object*> objs;
unordered_map <int, Object*> mrks;
void mark (void)
{
    for (auto [k, v:> : mrks)
    if (v != nullptr) v -> GC_mark ();
}

int main (void)
{
    initialize ();
    GarbageCollector::registry (mark);
    // StopWatch
    StopWatch* sw = new StopWatch (milisecond);
    sw -> start ();
    for (auto i=0; i<100000000; i++);
    cout << sw -> now () << endl;
    for (auto i=0; i<100; i++) objs [i] = mrks [i] = new Integer (i);
    for (auto i=0; i<100; i++)
    {
        int s;
        cout << "Remove an Object! " << flush;
        cin >> s;
        mrks.erase (s);
        GarbageCollector::start ();
        cout << "GC started" << endl;
        GarbageCollector::clear ();
        cout << "GC finished" << endl;
        cout << "Query: " << flush;
        cin >> s;
        if (objs [s] == nullptr) cout << "Cleared!" << endl;
        else
        {
            auto p = objs [s];
            if (not isinstance (p, dysprosium.Integer)) cout << "Number: " << (((Integer*) (p)) -> get ()) << endl;
            else cout << "This is not a dysprosium.Integer!" << endl;
        }
    }
}
